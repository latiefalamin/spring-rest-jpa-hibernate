package com.secondstack.training.spring.jpa.hibernate.service;

import com.secondstack.training.spring.jpa.hibernate.domain.Subject;
import com.secondstack.training.spring.jpa.hibernate.domain.Teacher;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public interface SubjectService {
    void save(Subject subject);
    Subject update(Integer id, Subject subject);
    void delete(Subject subject);
    void delete(Integer id);
    List<Subject> findAll();
    Subject findById(Integer id);
    List<Subject> findByTeacher(Teacher teacher);

}
