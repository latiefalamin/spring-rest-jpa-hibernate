package com.secondstack.training.spring.jpa.hibernate.service.impl;

import com.secondstack.training.spring.jpa.hibernate.domain.Teacher;
import com.secondstack.training.spring.jpa.hibernate.service.TeacherService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class TeacherServiceImpl implements TeacherService {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void save(Teacher teacher) {
        entityManager.persist(teacher);
    }

    @Override
    public Teacher update(Integer id, Teacher teacher){
        Teacher oldTeacher = findById(id);
        oldTeacher.setAcademicDegree(teacher.getAcademicDegree());
        oldTeacher.setAddress(teacher.getAddress());
        oldTeacher.setFirstName(teacher.getFirstName());
        oldTeacher.setLastName(teacher.getLastName());
        oldTeacher.setSex(teacher.getSex());
        return entityManager.merge(oldTeacher);
    }

    @Override
    public void delete(Teacher teacher){
        entityManager.remove(teacher);
    }

    @Override
    public void delete(Integer id){
        entityManager.remove(entityManager.find(Teacher.class, id));
    }

    @Override
    public List<Teacher> findAll(){
        List<Teacher> results = entityManager.createQuery("SELECT o from Teacher o", Teacher.class).getResultList();
        return results;
    }
    @Override
    public Teacher findById(Integer id){
        return entityManager.find(Teacher.class, id);
    }
}
