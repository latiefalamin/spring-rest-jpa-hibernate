package com.secondstack.training.spring.jpa.hibernate.controller.web;

import com.secondstack.training.spring.jpa.hibernate.domain.Lecture;
import com.secondstack.training.spring.jpa.hibernate.service.LectureService;
import com.secondstack.training.spring.jpa.hibernate.service.SubjectService;
import com.secondstack.training.spring.jpa.hibernate.service.TeacherService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("lecture")
public class LectureController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private LectureService lectureService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private TeacherService teacherService;

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String form(ModelMap modelMap) {
        logger.debug("Received request to get form lecture");

        Lecture lecture = new Lecture();
        modelMap.addAttribute("lecture", lecture);
        modelMap.addAttribute("lectureUrl", "/lecture");
        modelMap.addAttribute("subjectList", subjectService.findAll());
        modelMap.addAttribute("teacherList", teacherService.findAll());
        modelMap.addAttribute("menuLectureClass", "active");

        return "lecture-form-tiles";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@ModelAttribute("lecture") Lecture lecture, ModelMap modelMap){
        logger.debug("Received request to create lecture");

        lectureService.save(lecture);
        modelMap.addAttribute("lecture", lectureService.findById(lecture.getId()));
        modelMap.addAttribute("menuLectureClass", "active");

        return "lecture-data-tiles";
    }

    @RequestMapping(value = "/form", params = "id", method = RequestMethod.GET)
    public String form(@RequestParam("id")Integer id, ModelMap modelMap){
        logger.debug("Received request to get form lecture");

        Lecture lecture = lectureService.findById(id);
        modelMap.addAttribute("lecture", lecture);
        modelMap.addAttribute("lectureUrl", "/lecture?id=" + id);
        modelMap.addAttribute("subjectList", subjectService.findAll());
        modelMap.addAttribute("teacherList", teacherService.findAll());
        modelMap.addAttribute("menuLectureClass", "active");

        return "lecture-form-tiles";
    }

    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id")Integer id, @ModelAttribute("lecture") Lecture lecture, ModelMap modelMap){
        logger.debug("Received request to update lecture");

        lectureService.update(id, lecture);
        modelMap.addAttribute("lecture", lectureService.findById(lecture.getId()));
        modelMap.addAttribute("menuLectureClass", "active");

        return "lecture-data-tiles";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String findAll(ModelMap modelMap){
        logger.debug("Received request to get list lecture");

        List<Lecture> lectureList = lectureService.findAll();
        modelMap.addAttribute("lectureList", lectureList);
        modelMap.addAttribute("menuLectureClass", "active");

        return "lecture-list-tiles";
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    public String findById(@RequestParam("id")Integer id, ModelMap modelMap){
        logger.debug("Received request to get data lecture");

        Lecture lecture = lectureService.findById(id);
        modelMap.addAttribute("lecture", lecture);
        modelMap.addAttribute("menuLectureClass", "active");

        return "lecture-data-tiles";
    }

    @RequestMapping(value = "/delete", params = {"id"}, method = RequestMethod.GET)
    public String delete(@RequestParam("id")Integer id, ModelMap modelMap){
        logger.debug("Received request to delete lecture");

        lectureService.delete(id);

        return "redirect:/lecture";
    }

}
