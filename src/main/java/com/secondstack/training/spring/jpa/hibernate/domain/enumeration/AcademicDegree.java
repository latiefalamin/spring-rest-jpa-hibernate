package com.secondstack.training.spring.jpa.hibernate.domain.enumeration;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/25/13
 * Time: 9:58 AM
 * To change this template use File | Settings | File Templates.
 */
public enum AcademicDegree {
    S1, S2, S3, PROF
}
