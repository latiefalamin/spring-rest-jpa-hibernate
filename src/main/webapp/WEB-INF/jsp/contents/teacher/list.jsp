<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>
            Teacher List
            <a href="<c:url value='/teacher/form'/>">
                <button class="btn btn-small">
                    <i class="icon-plus"></i>
                </button>
            </a>
        </h4>
    </div>

    <table class="table table-striped">

        <thead>
        <td>Id</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Academic Degree</td>
        <td>Address</td>
        <td>Sex</td>
        <td>Action</td>
        </thead>

        <tbody>

        <c:forEach items="${teacherList}" var="teacher">
            <tr>
                <td><a href="<c:url value='/teacher?id=${teacher.id}'/>">${teacher.id}</a></td>
                <td>${teacher.firstName}</td>
                <td>${teacher.lastName}</td>
                <td>${teacher.academicDegree}</td>
                <td>${teacher.address}</td>
                <td>${teacher.sex}</td>
                <td>
                    <a href="<c:url value='/teacher/form?id=${teacher.id}'/>">
                        <button class="btn btn-primary">
                            <i class="icon-white icon-edit"></i>
                        </button>
                    </a>
                    <a href="<c:url value='/teacher/delete?id=${teacher.id}'/>">
                        <button class="btn btn-primary btn-danger">
                            <i class="icon-white icon-remove"></i>
                        </button>
                    </a>
                </td>
            </tr>
        </c:forEach>

        </tbody>

    </table>

</div>

<script type="text/javascript">
    var tagRow = "<tr>" +
            "   <td><a href=\"<c:url value='/teacher?id=<teacher.id>'/> \"><teacher.id></a></td>" +
            "   <td><teacher.firstName></td>" +
            "   <td><teacher.lastName></td>" +
            "   <td><teacher.academicDegree></td>" +
            "   <td><teacher.address></td>" +
            "   <td><teacher.sex></td>" +
            "   <td>" +
            "       <a href=\"<c:url value='/teacher/form?id=<teacher.id>'/> \">" +
            "           <button class='btn btn-primary'>" +
            "               <i class='icon-white icon-edit'></i>" +
            "           </button>" +
            "       </a>" +
            "       <a href=\"<c:url value='/teacher/delete?id=<teacher.id>'/> \"> " +
            "           <button class='btn btn-primary btn-danger'>" +
            "               <i class='icon-white icon-remove'></i>" +
            "           </button>" +
            "       </a>" +
            "   </td>" +
            "</tr>"
    $(document).ready(function () {
        $('#buttonRefresh').click(function () {
            $.ajax({
                url:'<c:url value="/api/teacher" />',
                success:function (result, status, xhr) {
                    console.info(result, status, xhr);
                    var innerHtml = ""
                    for(var i=0;i<result.length;i++){
                        var o = result[i];
                        innerHtml = innerHtml +
                                tagRow.replace(/<teacher.id>/g, o.id +'')
                                        .replace('<teacher.firstName>', o.firstName)
                                        .replace('<teacher.lastName>', o.lastName)
                                        .replace('<teacher.address>', o.address)
                                        .replace('<teacher.sex>', o.sex)
                                        .replace('<teacher.academicDegree>', o.academicDegree);
                    }
                    $('tbody').html(innerHtml);
                }
            })
        });
    });
</script>