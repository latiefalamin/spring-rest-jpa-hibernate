package com.secondstack.training.spring.jpa.hibernate.service;

import com.secondstack.training.spring.jpa.hibernate.domain.Lecture;
import com.secondstack.training.spring.jpa.hibernate.domain.Subject;
import com.secondstack.training.spring.jpa.hibernate.domain.Teacher;
import com.secondstack.training.spring.jpa.hibernate.domain.enumeration.Semester;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:25 AM
 * To change this template use File | Settings | File Templates.
 */
public interface LectureService {
    void save(Lecture lecture);
    Lecture update(Integer id, Lecture lecture);
    void delete(Lecture lecture);
    void delete(Integer id);
    List<Lecture> findAll();
    Lecture findById(Integer id);

    List<Lecture> findByTeacher(Teacher teacher);
    List findSimpleByTeacher1(Teacher teacher);
    List findSimpleByTeacher2(Teacher teacher);
    List findSimpleByTeacher3(Teacher teacher);

    Lecture findBySubjectAndTeacherAndSemesterAndYear(Subject subject, Teacher teacher, Semester semester, Integer year);
}
