<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>This Subject Data</h4>
    </div>
    <table>
        <tr>
            <td>Code :</td>
            <td>${subject.code}</td>
        </tr>
        <tr>
            <td>Name :</td>
            <td>${subject.name}</td>
        </tr>
        <tr>
            <td>Sks :</td>
            <td>${subject.sks}</td>
        </tr>
    </table>
    <br>
    <a href="<c:url value='/subject'/>"><button class="btn">Back</button></a>
</div>
