<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>This Teacher Data</h4>
    </div>
    <table>
        <tr>
            <td>First Name :</td>
            <td>${teacher.firstName}</td>
        </tr>
        <tr>
            <td>Last Name :</td>
            <td>${teacher.lastName}</td>
        </tr>
        <tr>
            <td>Academic Degree :</td>
            <td>${teacher.academicDegree}</td>
        </tr>
        <tr>
            <td>Address :</td>
            <td>${teacher.address}</td>
        </tr>
        <tr>
            <td>Sex :</td>
            <td>${teacher.sex}</td>
        </tr>
    </table>
    <br>
    <a href="<c:url value='/teacher'/>"><button class="btn">Back</button></a>
</div>
