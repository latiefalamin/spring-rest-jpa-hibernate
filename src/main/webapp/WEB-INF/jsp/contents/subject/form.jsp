<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>Insert Subject Data</h4>
    </div>

    <form:form modelAttribute="subject" method="POST" action="${subjectUrl}" class="form-horizontal">
        <div class="control-group">
            <form:label class="control-label" path="code">Code</form:label>
            <div class="controls">
                <form:input type="text" path="code"></form:input>
            </div>
        </div>
        <div class="control-group">
            <form:label class="control-label" path="name">Name</form:label>
            <div class="controls">
                <form:input type="text" path="name"></form:input>
            </div>
        </div>
        <div class="control-group">
            <form:label class="control-label" path="sks">Sks</form:label>
            <div class="controls">
                <form:input type="text" path="sks"></form:input>
            </div>
        </div>

        <input class="btn" type="reset" value="Reset"/>
        <input class="btn btn-primary" type="submit" value="Submit"/>
    </form:form>
</div>
