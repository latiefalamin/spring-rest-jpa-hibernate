<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>
            Student List
            <a href="<c:url value='/student/form'/>">
                <button class="btn btn-small">
                    <i class="icon-plus"></i>
                </button>
            </a>
            <button id='buttonRefresh' class="btn btn-small">
                <i class="icon-refresh"></i>
            </button>

            <div class="btn-group">
                <a class="btn btn-small dropdown-toggle" data-toggle="dropdown" >
                    <i class="icon-download"></i>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="<c:url value='/student/report?format=pdf'/>">.pdf</a></li>
                    <li><a href="<c:url value='/student/report?format=xls'/>">.xls</a></li>
                    <li><a href="<c:url value='/student/report?format=csv'/>">.csv</a></li>
                    <li><a href="<c:url value='/student/report?format=html'/>">.html</a></li>
                </ul>
            </div>
        </h4>
    </div>

    <table class="table table-striped">

        <thead>
        <td>Id</td>
        <td>First Name</td>
        <td>Last Name</td>
        <td>Address</td>
        <td>Sex</td>
        <td>Age</td>
        <td>Action</td>
        </thead>

        <tbody>

        <c:forEach items="${studentList}" var="student">
            <tr>
                <td><a href="<c:url value='/student?id=${student.id}'/>">${student.id}</a></td>
                <td>${student.firstName}</td>
                <td>${student.lastName}</td>
                <td>${student.address}</td>
                <td>${student.sex}</td>
                <td>${student.age}</td>
                <td>
                    <a href="<c:url value='/student/form?id=${student.id}'/>">
                        <button class="btn btn-primary">
                            <i class="icon-white icon-edit"></i>
                        </button>
                    </a>
                    <a href="<c:url value='/student/delete?id=${student.id}'/>">
                        <button class="btn btn-primary btn-danger">
                            <i class="icon-white icon-remove"></i>
                        </button>
                    </a>
                </td>
            </tr>
        </c:forEach>

        </tbody>

    </table>

</div>

<script type="text/javascript">
    var tagRow = "<tr>" +
            "   <td><a href=\"<c:url value='/student?id=%student.id%'/> \">%student.id%</a></td>" +
            "   <td>%student.firstName%</td>" +
            "   <td>%student.lastName%</td>" +
            "   <td>%student.address%</td>" +
            "   <td>%student.sex%</td>" +
            "   <td>%student.age%</td>" +
            "   <td>" +
            "       <a href=\"<c:url value='/student/form?id=%student.id%'/> \">" +
            "           <button class='btn btn-primary'>" +
            "               <i class='icon-white icon-edit'></i>" +
            "           </button>" +
            "       </a>" +
            "       <a href=\"<c:url value='/student/delete?id=%student.id%'/> \"> " +
            "           <button class='btn btn-primary btn-danger'>" +
            "               <i class='icon-white icon-remove'></i>" +
            "           </button>" +
            "       </a>" +
            "   </td>" +
            "</tr>";
    $(document).ready(function () {

        $('.dropdown-toggle').dropdown();

        $('#buttonRefresh').click(function () {
            $.ajax(
                    {
                        url:'<c:url value="/api/student" />',
                        type:'GET',
                        timeout:1000,
                        success:function (result, status, xhr) {
                            console.info(result);
                            var innerHtml = "";
                            for (var i = 0; i < result.length; i++) {
                                var o = result[i];
                                innerHtml = innerHtml +
                                        tagRow.replace(/%student.id%/g, o.id + '')
//                                        tagRow.replace('%student.id%', o.id + '')
                                                .replace('%student.firstName%', o.firstName)
                                                .replace('%student.lastName%', o.lastName)
                                                .replace('%student.address%', o.address)
                                                .replace('%student.sex%', o.sex)
                                                .replace('%student.age%', o.age);
                            }
                            $('tbody').html(innerHtml);
                        },
                        error:function(xhr,status,error){

                        }
                    }
            );
        });
    });
</script>