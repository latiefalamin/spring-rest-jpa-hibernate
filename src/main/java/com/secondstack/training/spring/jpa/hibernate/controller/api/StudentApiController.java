package com.secondstack.training.spring.jpa.hibernate.controller.api;

import com.secondstack.training.spring.jpa.hibernate.domain.Student;
import com.secondstack.training.spring.jpa.hibernate.service.StudentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
//@RequestMapping(value = "student", headers = {"Accept=application/json"})
@RequestMapping(value = "/api/student")
public class StudentApiController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private StudentService studentService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Student student){
        logger.debug("Received rest request to create student");
        studentService.save(student);
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestParam("id")Integer id, @RequestBody Student student){
        logger.debug("Received rest request to update student");
        studentService.update(id, student);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public List<Student> findAll(){
        logger.debug("Received rest request to get list student");
        List<Student> studentList = studentService.findAll();
        return studentList;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public Student findById(@RequestParam("id")Integer id){
        logger.debug("Received rest request to get data student");
        Student student = studentService.findById(id);
        return student;
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@RequestParam("id")Integer id){
        logger.debug("Received rest request to delete student");
        studentService.delete(id);
    }

}
