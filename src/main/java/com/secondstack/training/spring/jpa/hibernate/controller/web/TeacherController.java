package com.secondstack.training.spring.jpa.hibernate.controller.web;

import com.secondstack.training.spring.jpa.hibernate.domain.Teacher;
import com.secondstack.training.spring.jpa.hibernate.service.TeacherService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("teacher")
public class TeacherController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private TeacherService teacherService;

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String form(ModelMap modelMap) {
        logger.debug("Received request to get form teacher");

        Teacher teacher = new Teacher();
        modelMap.addAttribute("teacher", teacher);
        modelMap.addAttribute("teacherUrl", "/teacher");
        modelMap.addAttribute("menuTeacherClass", "active");

        return "teacher-form-tiles";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@ModelAttribute("teacher") Teacher teacher, ModelMap modelMap){
        logger.debug("Received request to create teacher");

        teacherService.save(teacher);
        modelMap.addAttribute("teacher", teacher);
        modelMap.addAttribute("menuTeacherClass", "active");

        return "teacher-data-tiles";
    }

    @RequestMapping(value = "/form", params = "id", method = RequestMethod.GET)
    public String form(@RequestParam("id")Integer id, ModelMap modelMap){
        logger.debug("Received request to get form teacher");

        Teacher teacher = teacherService.findById(id);
        modelMap.addAttribute("teacher", teacher);
        modelMap.addAttribute("teacherUrl", "/teacher?id=" + id);
        modelMap.addAttribute("menuTeacherClass", "active");

        return "teacher-form-tiles";
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.POST)
    public String update(@RequestParam("id")Integer id, @ModelAttribute("teacher") Teacher teacher, ModelMap modelMap){
        logger.debug("Received request to update teacher");

        teacherService.update(id, teacher);
        modelMap.addAttribute("teacher", teacher);
        modelMap.addAttribute("menuTeacherClass", "active");

        return "teacher-data-tiles";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String findAll(ModelMap modelMap){
        logger.debug("Received request to get list teacher");

        List<Teacher> teacherList = teacherService.findAll();
        modelMap.addAttribute("teacherList", teacherList);
        modelMap.addAttribute("menuTeacherClass", "active");

        return "teacher-list-tiles";
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    public String findById(@RequestParam("id")Integer id, ModelMap modelMap){
        logger.debug("Received request to get data teacher");

        Teacher teacher = teacherService.findById(id);
        modelMap.addAttribute("teacher", teacher);
        modelMap.addAttribute("menuTeacherClass", "active");

        return "teacher-data-tiles";
    }

    @RequestMapping(value = "/delete", params = {"id"}, method = RequestMethod.GET)
    public String delete(@RequestParam("id")Integer id, ModelMap modelMap){
        logger.debug("Received request to delete teacher");

        teacherService.delete(id);

        return "redirect:/teacher";
    }

}
