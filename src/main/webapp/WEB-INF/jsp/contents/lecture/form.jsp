<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>Insert Lecture Data</h4>
    </div>

    <form:form modelAttribute="lecture" method="POST" action="${lectureUrl}" class="form-horizontal">

        <div class="control-group">
            <form:label class="control-label" path="subject.id">Subject</form:label>
            <div class="controls">
                <form:select type="text" path="subject.id">
                    <c:forEach items="${subjectList}" var='subject'>
                        <form:option value="${subject.id}">${subject.name}</form:option>
                    </c:forEach>
                </form:select>
            </div>
        </div>
        <div class="control-group">
            <form:label class="control-label" path="teacher.id">Teacher</form:label>
            <div class="controls">
                <form:select type="text" path="teacher.id">
                    <c:forEach items="${teacherList}" var='teacher'>
                        <form:option value="${teacher.id}">${teacher.firstName}</form:option>
                    </c:forEach>
                </form:select>
            </div>
        </div>
        <div class="control-group">
            <form:label class="control-label" path="semester">Semester</form:label>
            <div class="controls">
                <form:select type="text" path="semester">
                    <form:option value="ODD">ODD</form:option>
                    <form:option value="EVEN">EVEN</form:option>
                </form:select>
            </div>
        </div>
        <div class="control-group">
            <form:label class="control-label" path="year">Year</form:label>
            <div class="controls">
                <form:input type="text" path="year"></form:input>
            </div>
        </div>

        <input class="btn" type="reset" value="Reset"/>
        <input class="btn btn-primary" type="submit" value="Submit"/>
    </form:form>
</div>
